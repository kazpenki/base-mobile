<?php
/* パンくずナビ */
function get_breadcrumb_list($include_category = 1, $include_tag = 1, $include_taxonomy = 1)
{
// ベースパンくず(サイト名など)
$base_breadcrumb = '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.get_home_url().'" itemprop="url"><span itemprop="title">HOME</span></a></li>';
$top_url = get_home_url(null,'/');
// ループ外対応
global $query_string;
global $post;
query_posts($query_string);
if (have_posts()) : while(have_posts()) : the_post();
endwhile; endif;
// クエリリセットする
wp_reset_query();

// ページ数を取得(ページ数(0の場合は1ページ目なので1に変更する))
if(get_query_var('paged') == 0): $page = 1; else: $page = get_query_var('paged') ; endif;

// 投稿・固定ページかつアタッチメントページ以外の場合
if(is_singular() && !is_attachment())
{
// 記事についているカテゴリを全て取得する
$categories = get_the_category();
// カテゴリがある場合に実行する
if(!empty($categories))
{
$category_count = count($categories);
$loop = 1;
$category_list = '';
foreach($categories as $category)
{
// 値が1だったら、URLにカテゴリーのタクソノミーを含めて変数に格納する。
if($include_category === 1)
{
$category_list .= '<a href="'.$top_url.esc_html($category->taxonomy).'/'.esc_html($category->slug).'/" itemprop="url"><span itemprop="title">'.esc_html($category->name).'</span></a>';
}
// 値が指定されていないか1以外だったら、URLにカテゴリーのタクソノミーを含めないで変数に格納する。
else
{
$category_list .= '<a href="'.$top_url.esc_html($category->slug).'/" itemprop="url"><span itemprop="title">'.esc_html($category->name).'</span></a>';
}
// ループの最後でない場合のみスラッシュ追加（複数カテゴリ対応）
if($loop != $category_count)
{
$category_list .= '?/?';
}
++$loop;
}
}
// カテゴリがない場合はNULLを格納する
else
{
$category_list = null;
}

// 記事についているタグを全て取得する
$tags = get_the_tags();
// タグがあれば実行する
if(!empty($tags))
{
$tags_count = count($tags);
$loop=1;
$tag_list = '';
foreach($tags as $tag)
{
// 値が1だったら、URLにタグのタクソノミーを含めて変数に格納する。
if($include_tag === 1)
{
$tag_list .= '<a href="'.$top_url.esc_html($tag->taxonomy).'/'.esc_html($tag->slug).'/" itemprop="url"><span itemprop="title">'.esc_html($tag->name).'</span></a>';
}
// 値が指定されていないか1以外だったら、URLにタグのタクソノミーを含めないで変数に格納する。
else
{
$tag_list .= '<a href="'.$top_url.esc_html($tag->slug).'/" itemprop="url"><span itemprop="title">'.esc_html($tag->name).'</span></a>';
}
// ループの最後でない場合のみスラッシュ追加（複数タグ対応）
if($loop !== $tags_count)
{
$tag_list .= '?/?';
}
++$loop;
}
}
// タグがない場合はNULLを格納する
else
{
$tag_list = null;
}

// タクソノミーを全て取得する
$taxonomies = get_the_taxonomies();
// タクソノミーがある場合に実行する
if(!empty($taxonomies))
{
$term_list = '';
$taxonomies_count = count($taxonomies);
$taxonomies_loop = 1;
foreach(array_keys($taxonomies) as $key)
{
// タームを取得
$terms = get_the_terms($post->ID, $key);
$terms_count = count($terms);
$loop = 1;
foreach($terms as $term)
{
// 値が1だったら、URLにタクソノミーを含めて変数に格納する。
if($include_taxonomy === 1)
{
$term_list .= '<a href="'.$top_url.esc_html($term->taxonomy).'/'.esc_html($term->slug).'/" itemprop="url"><span itemprop="title">'.esc_html($term->name).'</span></a>';
}
// 値が指定されていないか1以外だったらURLにタクソノミーを含めないで変数に格納する。
else
{
$term_list .= '<a href="'.$top_url.esc_html($term->slug).'/" itemprop="url"><span itemprop="title">'.esc_html($term->name).'</span></a>';
}
// ループの最後でない場合のみスラッシュ追加（複数ターム対応）
if($loop != $terms_count)
{
$term_list .= '?/?';
}
++$loop;
}
// ループの最後でない場合のみスラッシュ追加（複数タクソノミー対応）
if($taxonomies_loop != $taxonomies_count)
{
$term_list .= '?/?';
}
++$taxonomies_loop;
}
}
// タクソノミーがない場合はNULLを格納する
else
{
$term_list = null;
}
}

// 基本パンくずリストを変数に格納する。
$breadcrumb_lists = $base_breadcrumb;

// ホームの場合
if(is_home())
{
}
// カスタム投稿タイプアーカイブの場合
elseif(is_post_type_archive())
{
// ページ数が1より大きい場合(○ページ目)を追加して格納する
if($page > 1)
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(get_post_type_object( get_post_type() )->label).'の記事一覧('.$page.'ページ目)</li>';
}
// ページ数が1の場合は(○ページ目)はなしで格納する
else
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(get_post_type_object( get_post_type() )->label).'の記事一覧</li>';
}
}
// カスタム投稿タイプ以外のアーカイブの場合
elseif(is_archive())
{
// 年アーカイブの場合
if(is_year())
{
// ページ数が1より大きい場合(○ページ目)を追加して格納する
if($page > 1)
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(get_the_time("Y年")).'の記事一覧('.$page.'ページ目)</li>';
}
// ページ数が1の場合は(○ページ目)はなしで格納する
else
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(get_the_time("Y年")).'の記事一覧</li>';
}
}
// 月アーカイブの場合
elseif(is_month())
{
// ページ数が1より大きい場合(○ページ目)を追加して格納する
if($page > 1)
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(get_the_time("Y年m月")).'の記事一覧('.$page.'ページ目)</li>';
}
// ページ数が1の場合は(○ページ目)はなしで格納する
else
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(get_the_time("Y年m月")).'の記事一覧</li>';
}
}
// 日アーカイブの場合
elseif(is_day())
{
// ページ数が1より大きい場合(○ページ目)を追加して格納する
if($page > 1)
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(get_the_time("Y年m月d日")).'の記事一覧('.$page.'ページ目)</li>';
}
// ページ数が1の場合は(○ページ目)はなしで格納する
else
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(get_the_time("Y年m月d日")).'の記事一覧</li>';
}
}
// カテゴリの場合
elseif(is_category())
{
// ページ数が1より大きい場合(○ページ目)を追加して格納する
if($page > 1)
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(single_cat_title('',false)).'の記事一覧('.$page.'ページ目)</li>';
}
// ページ数が1の場合は(○ページ目)はなしで格納する
else
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(single_cat_title('',false)).'の記事一覧</li>';
}
}
// タグの場合
elseif(is_tag())
{
// ページ数が1より大きい場合(○ページ目)を追加して格納する
if($page > 1)
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(single_tag_title('',false)).'の記事一覧('.$page.'ページ目)</li>';
}
// ページ数が1の場合は(○ページ目)はなしで格納する
else
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(single_tag_title('',false)).'の記事一覧</li>';
}
}
// タクソノミー(カスタム分類)の場合
elseif(is_tax())
{
// ページ数が1より大きい場合(○ページ目)を追加して格納する
if($page > 1)
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(single_term_title('',false)).'の記事一覧('.$page.'ページ目)</li>';
}
// ページ数が1の場合は(○ページ目)はなしで格納する
else
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(single_term_title('',false)).'の記事一覧</li>';
}
}
}
// 投稿ページ
elseif(is_single())
{
// ページ数を取得(ページ数(0の場合は1ページ目なので1に変更する))
if(get_query_var('page') == 0): $page = 1; else: $page = get_query_var('page') ; endif;
// 通常投稿の場合
if(get_post_type() === 'post')
{
// カスタムフィールドの値を取得
$seo_title = esc_html(get_post_meta($post->ID, 'seo_title', true));

// カテゴリがある場合のみ追加
if(!empty($category_list))
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.$category_list.'</li>';
}

// 2ページ目以降の場合
if($page > 1)
{
// カスタムフィールドに値があったらその値を格納する
if(!empty($seo_title))
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.$seo_title.'</li>';
}
// カスタムフィールドに値がなかった場合
else
{
// ページが分割されている場合のタイトル取得
if(function_exists('get_current_split_string'))
{
$split_title = esc_html(get_current_split_string($page));
}
else
{
$split_title = null;
}

// ページが分割されている場合のタイトルがあった場合はそれを加える
if(!empty($split_title))
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(get_the_title()).'【'.$split_title.'】</li>';
}
// それ以外は(○ページ目)を加える
else
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(get_the_title()).'('.$page.'ページ目)</li>';
}
}
}
// 1ページ目の場合
else
{
// カスタムフィールドに値があったらその値を格納する
if(!empty($seo_title))
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.$seo_title.'</li>';
}
// カスタムフィールドに値がなかった場合
else
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(get_the_title()).'</li>';
}
}
}
// カスタム投稿の場合
else
{
// タームがある場合のみ追加
if(!empty($term_list))
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.$term_list.'</li>';
}
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(get_the_title()).'</li>';
}
}
// 固定ページ
elseif(is_page())
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.esc_html(get_the_title()).'</li>';
}
// 検索結果
elseif(is_search())
{
// ページ数が1より大きい場合(○ページ目)を追加して格納する
if($page > 1)
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">「'.esc_html(get_search_query()).'」で検索した結果('.$page.'ページ目)</li>';
}
// ページ数が1の場合は(○ページ目)はなしで格納する
else
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">「'.esc_html(get_search_query()).'」で検索した結果</li>';
}
}
// 404ページの場合
elseif(is_404())
{
$breadcrumb_lists .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">お探しのページは見つかりませんでした</li>';
}
else
{
$breadcrumb_lists = $base_breadcrumb;
}
// パンくずリスト成形
if(!empty($breadcrumb_lists))
{
$breadcrumb_lists = '<ul class="bread_ul">'.$breadcrumb_lists.'</ul>';
}
return $breadcrumb_lists;
}
?>