<?php

remove_action('wp_head', 'wp_generator');
remove_action('wp_head','rsd_link');  
// remove_action('wp_head','rel_canonical');
remove_action('wp_head','index_rel_link'); 
remove_action('wp_head','adjacent_posts_rel_link_wp_head'); 
remove_action('wp_head','parent_post_rel_link',10,0);	
remove_action('wp_head','start_post_rel_link',10,0); 
remove_action('wp_head','wp_shortlink_wp_head'); 
remove_action('wp_head','feed_links_extra',3); 
remove_action('wp_head','index_rel_link'); 

//emoji
function disable_emojis() {
     remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
     remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
/*
     remove_action( 'wp_print_styles', 'print_emoji_styles' );
     remove_action( 'admin_print_styles', 'print_emoji_styles' );     
     remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
     remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );  
     remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
     add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
*/
}
add_action( 'init', 'disable_emojis' );

//タイトル
function nendebcom_theme_slug_setup() {
   add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'nendebcom_theme_slug_setup' );

/*
 * タイトルの区切り線を | にする
 *
 * @since WordPress 4.4.0
 * License: GPLv2 or later
 */
function nendebcom_title_separator( $sep ){
    $sep = '|';
    return $sep;
}
add_filter( 'document_title_separator', 'nendebcom_title_separator' );


//カスタムメニュー
register_nav_menus( array(
	'navigation' => 'メインメニュー',
	'sbmenu' => 'スマホ用メニュー'
	));

/*	Register sidebars
/*---------------------------------------------------------*/
register_sidebar(array(
	'name' => __( '通常サイドバー','bootstraptheme' ),
	'id'	=>	'sidebar-1',
	'description'	=>	__('サイドメニューを表示'),
  'before_widget' => '<div><section id="%1$s" class="widget %2$s">',
  'after_widget' => '</section></div>',
  'before_title' => '<h3>',
  'after_title' => '</h3>',
	));

register_sidebar(array(
	'name' => __( 'スマホサブメニュー','bootstraptheme' ),
	'id'	=>	'sidebar-2',
	'description'	=>	__('サイドメニューを表示'),
  'before_widget' => '<div><section id="%1$s" class="widget %2$s">',
  'after_widget' => '</section></div>',
  'before_title' => '<h3>',
  'after_title' => '</h3>',
	));

// Register thumbnails
add_theme_support( 'post-thumbnails' );

add_image_size( 'twentyseventeen-featured-image', 300, 300, true );
add_image_size( 'post_header_image', 1200, 300, true );


// Register pagenation
function pagination($pages = '', $range = 4)
{
     $showitems = ($range * 2)+1;  
 
     global $paged;
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
 
     if(1 != $pages)
     {
         echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
             }
         }
 
         if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
         echo "</div>\n";
     }
}



/**
 * breadcrumb functions and filters.
 */

require get_parent_theme_file_path( '/inc/breadcrumb_functions.php' );

/**
 * customfeeld キーワード　h1 functions and filters.
 */

//require get_parent_theme_file_path( '/inc/customfeeld_functions.php' );

/**
 * Js functions and filters.
 */

function add_scripts() {

wp_enqueue_script( 'matome', get_template_directory_uri() . '/assets/javascripts/matome.js', array(), '3.3.7', true );
/*
wp_enqueue_script( 'bootstrap3', get_template_directory_uri() . '/assets/javascripts/bootstrap.min.js', array( 'jquery' ), '3.3.7', true );
wp_enqueue_script( 'js_machHeight', get_template_directory_uri() . '/assets/javascripts/jquery.matchHeight-min.js', array( 'jquery' ), '20170228', true );
wp_enqueue_script( 'footerFix', get_template_directory_uri() . '/assets/javascripts/footerFixed.js', array( 'jquery' ), '20170228', true );
wp_enqueue_script( 'MyScript', get_template_directory_uri() . '/assets/javascripts/my.js', array( 'jquery' ), '20170228', true );
wp_enqueue_script( 'Totop', get_template_directory_uri() . '/assets/javascripts/to-top.js', array( 'jquery' ), '20170301', true );
*/
}
// singleページ専用JS
//if ( is_single() ) wp_enqueue_script( 'smart-single-script', get_template_directory_uri() . '/js/single.js', "", '20160608', true );
add_action('wp_print_scripts', 'add_scripts');
function replace_script_tag ( $tag ) {
    return str_replace( "type='text/javascript'", 'async ', $tag );
}
add_filter( 'add_scripts', 'wp_print_scripts' );
function remove_script_type($tag) {
	return str_replace("type='text/javascript' ", 'async ', $tag);
}
add_filter('script_loader_tag','remove_script_type');
/**
 * WordPress本体のjquery.jsを読み込ませないで指定のJqueryを設定
 */
function add_files() {
// WordPress本体のjquery.jsを読み込まない
wp_deregister_script('jquery');
// jQueryの読み込み
wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js',  array(), "20170228", false );
wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/stylesheets/styles.css', "", '20170228' );
}
add_action( 'wp_enqueue_scripts', 'add_files' );
add_filter( 'style_loader_tag', 'replace_link_stylesheet_tag' );
function replace_link_stylesheet_tag($tag) {
	return preg_replace( array( "/'/", '/(id|type|media)=".+?" */', '/ \/>/' ), array( '"', '', '>' ), $tag );
}
//カテゴリー説明文でHTMLタグを使う
remove_filter( 'pre_term_description', 'wp_filter_kses' );
