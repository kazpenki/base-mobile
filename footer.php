<div class="footer_wrap" id="footer">
<div class="container">
	<footer>
		<p class="copyright">Copyright &copy; <?php echo date('Y'); ?><br><a href="<?php echo home_url(); ?>"><?php echo get_bloginfo('name') ?></a> <br>All right reserved.</p>
	</footer>

</div>
</div>

<?php wp_footer(); ?>
<?php get_template_part('include/snsbtn_script'); ?>
</body>
</html>