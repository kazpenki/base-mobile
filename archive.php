<?php get_header(); ?>
<body <?php body_class(); ?>>
<?php get_template_part('template-parts/head/head','block'); ?>
<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
<?php if ( '' !== get_the_post_thumbnail()) : ?>
<div class="post_header_image" style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>)">
</div>
<?php endif; ?>
<div class="container">
	<?php if ( have_posts() ) : ?>
		<header class="page-header">
			<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="taxonomy-description">', '</div>' );
			?>
		</header><!-- .page-header -->
	<?php endif; ?>
</div>
<div class="container">
	<div class="main_content">
			<div class="content">
				<?php
					if ( have_posts() ) :

						/* Start the Loop */
						while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/post/content-excerpt', get_post_format() );
						endwhile;
							if (function_exists("pagination")) {
								pagination($additional_loop->max_num_pages);
							};
					
						else :
							get_template_part( 'template-parts/post/content', 'none' );
						endif;
					?>
							
							
			</div>
	</div>
</div>
<div class="container side_container">
	<div class="content_side">
	<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer(); ?>