<?php get_header(); ?>
<body <?php body_class(); ?>>
<body <?php body_class(); ?>>
<?php get_template_part('template-parts/head/head','block'); ?>
<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>


<div class="container">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/page/content', 'sitemap' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div>

<?php get_footer(); ?>
</body>
</html>