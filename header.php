<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php //指定ページにnoindexタグを出力
if( (is_archive() && !is_category()) || //カテゴリー以外のアーカイブ系ページ
  is_tag() || //タグページ
  is_paged() || //ページの2ページ目以降
  is_search() || //検索結果
  is_attachment() || //画像attchement
  (is_page('ページID') || in_category("カテゴリースラッグ")) //指定の投稿・ページ・カテゴリの記事
 ):
?>
<meta name="robots" content="noindex">
<?php endif; ?>

<?php wp_head(); ?>

<meta name="keywords" content="<?php echo $keywords ?>">
<meta property='og:locale' content='ja_JP'>
<meta property='fb:admins' content=''>
<meta property='article:publisher' content='' />
<?php if(is_single()){ // 投稿記事 ?>
<meta property='og:type' content='article'>
<meta property='og:title' content='<?php the_title() ?>'>
<meta property='og:url' content='<?php the_permalink() ?>'>
<meta property='og:description' content='<?php echo mb_substr(get_the_excerpt(), 0, 100) ?>'>
<?php } else { //ホーム・カテゴリー・固定ページなど ?>
<meta property='og:type' content='website'>
<meta property='og:title' content='<?php bloginfo('name') ?>'>
<meta property='og:url' content='<?php bloginfo('url') ?>'>
<meta property='og:description' content='<?php echo get_bloginfo('description') ?>'>
<?php } ?>
<meta property='og:site_name' content='<?php echo get_bloginfo('name') ?>'>
<?php
  if (is_single() or is_page()){//投稿記事か固定ページ
    if (has_post_thumbnail()){//アイキャッチがある場合
       $image_id = get_post_thumbnail_id();
       $image = wp_get_attachment_image_src($image_id, 'full');
       echo '<meta property="og:image" content="'.$image[0].'">';echo "\n";
    } elseif( preg_match( '/<img.*?src=(["\'])(.+?)\1.*?>/i', $post->post_content, $imgurl ) && !is_archive()) {//アイキャッチ以外の画像がある場合
       echo '<meta property="og:image" content="'.$imgurl[2].'">';echo "\n";
    } else {//画像が1つも無い場合
       echo '<meta property="og:image" content="【デフォルト画像のURL】">';echo "\n";
    }
  } else { //ホーム・カテゴリーページなど
     echo '<meta property="og:image" content="【デフォルト画像のURL】">';echo "\n";
  }
?>
<!--Twtter Cards-->
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@kazpenki">
<meta name="twitter:creator" content="@kazpenki">
<meta name="twitter:domain" content="https://twitter.com/kazpenki" />
<?php get_template_part('include/analytics'); ?>
</head>

