<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="excerpt_box">
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</header><!-- .entry-header -->
	<div class="excerpt_box_wrap">
		<?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
		<div class="post-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'twentyseventeen-featured-image' ); ?>
			</a>
		</div><!-- .post-thumbnail -->
	<?php endif; ?>
		<?php the_excerpt(); ?>
	</div><!-- .panel-body -->
	<div class="excerpt-footer">
		<div class="text-right date-cat">
			<span class=""><?php the_time('Y/m/d')?>&nbsp;更新&nbsp;&nbsp;</span>

			<?php
			$posttags = get_the_category();
			$homeurl = home_url();
			if ($posttags) {
			foreach($posttags as $tag) {
			echo '<span class=""><a href="' . $homeurl . '/category/' . $tag->slug . '" class="' . $tag->slug . '">' . $tag->name . '</a>のトピックス</span>';
			}} ?>
			</div>
	</div>
</div>
</article><!-- #post-## -->
