<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header page-header">
		<h2 class="title"><?php echo get_the_title(); ?></h2>

			<div class="text-right date-cat">
			<span class=""><?php the_time('Y/m/d')?>&nbsp;更新&nbsp;&nbsp;</span><br>

			<?php
			$posttags = get_the_category();
			$homeurl = home_url();
			if ($posttags) {
			foreach($posttags as $tag) {
			echo '<span class=""><a href="' . $homeurl . '/category/' . $tag->slug . '" class="' . $tag->slug . '">' . $tag->name . '</a>のトピックス</span>';
			}} ?>
			</div>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading<span class="sr-only"> "%s"</span>', 'twentyseventeen' ),
				get_the_title()
			) );

			wp_link_pages( array(
				'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
				'after'       => '</div>',
				'link_before' => '<span class="page-number">',
				'link_after'  => '</span>',
			) );
		?>
	</div><!-- .entry-content -->
</article>
<div class="mt50 text-center">
<?php get_template_part('include/snsbtn_design01');//ソーシャルメディアボタン ?>
</div>