<div id="share">
<ul class="clearfix">
<li class="facebook"><a href="http://www.facebook.com/share.php?u=<?php echo get_permalink() ?>" onclick="window.open(this.href, 'window', 'width=550, height=450,personalbar=0,toolbar=0,scrollbars=1,resizable=1'); return false;">シェア</a></li>
<li class="twitter"><a class="button-twitter" href="http://twitter.com/intent/tweet?text=<?php echo trim(wp_title( '', false)) ?>&nbsp;|&nbsp;&amp;url=<?php echo get_permalink() ?>" onclick="window.open(encodeURI(decodeURI(this.href)), 'tweetwindow', 'width=550, height=450, personalbar=0, toolbar=0, scrollbars=1, resizable=1' ); return false;" target="_blank">ツイート</a></li>
<li class="googleplus"><a href="javascript:(function(){window.open('https://plusone.google.com/_/+1/confirm?hl=ja&url=<?php echo get_permalink() ?>'+encodeURIComponent(location.href)+'&title='+encodeURIComponent(document.title),'_blank');})();">+1</a></li>
</ul>
</div><!-- / #share -->