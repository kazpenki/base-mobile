<?php get_header(); ?>
<body <?php body_class(); ?>>
<?php get_template_part('template-parts/head/head','block'); ?>
<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
<?php if ( '' !== get_the_post_thumbnail()) : ?>
<div class="post_header_image" style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>)">
</div>
<?php endif; ?>
<div class="breadcrumb">
	<?php echo get_breadcrumb_list(); ?>
</div>
<div class="container">
<div class="main_content">
<?php get_template_part('include/snsbtn');//ソーシャルメディアボタン ?>
<div class="content">
<main id="main" class="site-main" role="main">

			<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/post/content', 'single' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

					
					

				endwhile; // End of the loop.
			?>
			<nav class="post-navigation">
					<h2 class="text-center">ナビーゲーション</h2>
				<div class="nav-links">
				<div class="nav-previous">
					<span class="sr-only">前の投稿</span>
					<?php previous_post_link( '%link','<span aria-hidden="true" class="nav-subtitle">前</span><span class="nav-title">%title</span>',false); ?>
				</div>          
				<div class="nav-next">
					<span class="sr-only">次の投稿</span>
					<?php next_post_link( '%link', '<span aria-hidden="true" class="nav-subtitle">次</span><span class="nav-title">%title</span>',false); ?>
				</div>
				</div>
			</nav>
			<nav class="post-navigation">
					<h2 class="text-center">カテゴリー内のナビーゲーション</h2>
				<div class="nav-links">
				<div class="nav-previous">
					<span class="sr-only">前の投稿</span>
					<?php previous_post_link( '%link','<span aria-hidden="true" class="nav-subtitle">前</span><span class="nav-title">%title</span>',TRUE); ?>
				</div>          
				<div class="nav-next">
					<span class="sr-only">次の投稿</span>
					<?php next_post_link( '%link', '<span aria-hidden="true" class="nav-subtitle">次</span><span class="nav-title">%title</span>',TRUE); ?>
				</div>
				</div>
			</nav>

		</main><!-- #main -->
</div>
</div>
</div>
<div class="container side_container">
<div class="content_side">
<?php get_sidebar(); ?>
</div>
</div>
<?php get_footer(); ?>
</body>
</html>