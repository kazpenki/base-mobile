<?php get_header(); ?>
<body <?php body_class(); ?>>
<?php get_template_part('template-parts/head/head','block'); ?>
<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
<div class="container">
	<div class="main_content">
		<div class="content">
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">
		
					<section class="error-404 not-found">
						<header class="page-header">
							<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentyseventeen' ); ?></h1>
						</header><!-- .page-header -->
						<div class="page-content">
							<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentyseventeen' ); ?></p>
		
							<?php get_search_form(); ?>
		
						</div><!-- .page-content -->
					</section><!-- .error-404 -->
				</main><!-- #main -->
			</div><!-- #primary -->
		</div>
	</div>
</div>
<div class="container side_container">
	<div class="content_side">
	<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer(); ?>